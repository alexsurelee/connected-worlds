# ENGR 301 Project 04, Connected Worlds, Project Proposal and Requirements Document

#### Alexander Lee, Raven Gotz-Tier, Victor Ong, Geordie Rogers, Angitha Ramesh

## 1. Introduction

This document outlines the requirements for the Connected Worlds project.
This document will outline what the application will do and what constraints it will meet.

### Client

**Matt Plummer** \
Information Technology Services \
matt.plummer@vuw.ac.nz

### 1.1 Purpose

Connected Worlds is an interactive online tool that can be used to visualise relationships and
pathways, collate digital resources and showcase research projects and tools.

### 1.2 Scope

Connected Worlds is a web based application that visualises the relationships between people, projects, tools and groups.
It will be able to run on both mobile and desktop devices and will function as an ecosystem that can be used to view
relationships between different elements for a wide range of use cases. During this project we aim to complete the
following goals which are expanded on throughout this document:

- Strengthen the existing prototype with testing and continuous integration
- Optimise the site for mobile user
- Extend Connected Worlds to be modular and use templates
  - i.e. allowing dynamic addition and removal of certain node types from view modes, defining which nodes should be central, etc.
- Expand the data available by harnessing additional data from APIs

Connected Worlds will help users find others that use similar tools, explore learning pathways, and discover
cross-disciplinary projects.

### 1.3 Product overview

#### 1.3.1 Product perspective

Connected Worlds is a stand-alone application that can be used for visualising data, specifically relationship data.
Connected Worlds can be configured to work both with without any relationships to other products.

Connected Worlds will primarily connect via the hardware interfaces provided by the device that the user is using to interact with the website. This could be the touchscreen of a phone or tablet, or a keyboard and mouse of a computer. Connected Worlds will be able to accept input from either of these interfaces and provide output through the screen on either device as well to communicate information to the user.

Connected Worlds will need to connect with API endpoints in order to collect additional information about staff, projects and faculties.
There are currently two planned APIs that Connected Worlds will need to connect to:

- **The Altmetric API:** Provides data for research impact. It measures the amount of time projects are mentioned and cited
  across the web and in other articles and projects. Altmetric outlines very specific requirements for being able to specifically make calls to
  their API. Connect Worlds will need to follow the following rules to make API Calls:

- Provide a version number in the format `https://api.altmetric.com/v1/`
- Use a valid API key `https://api.altmetric.com/v1/id/12345?key=xxx` that we will need to apply for
- Understand the response codes `200 (OK)`, `403 (invalid api key)`, `404 (no data)`, `429 (limit reached)` and
  `502 (down for maintanince)`
- **The ORCID API:** Identifies research and researchers. You can then use their ORCID IDs to follow thm and the research they do.
  ORCID also lays out details about how you need to construct calls to it's API and Connected Worlds will also have to be able to
  follow these rules:
- Use a valid secrets identity `?client_id=xxx` and `?client_secret=xxx`
- One of the API commands such as `/records`, `/works` [etc](https://members.orcid.org/api/tutorial/read-orcid-records).
- Understand the response codes `200 (OK)`, `404 (no data)`, `403 (syntax error)` [etc.](https://members.orcid.org/api/resources/error-codes)

Connected Worlds is planned to become open-sourced, which will enable a greater scope of use-cases for the project, mapping connections between much more complex datasets than simply research interests. This allows for great expansion on the project, which could result in a much greater scope in further iterations of its lifetime. The project may also be expanded to allow support for embedding in other pages, which will allow for further use-cases.

#### 1.3.2 Product functions

##### Primary

- Reading data from provided files
  - The system should be able to read data through from provided `.csv` files and turn this data into interconnected nodes.
- User interface for adding data
  - It should be possible for a user to open a prompt which will request all the relevant fields to add a node to the graph.
  - It should also be possible for a user to follow a system to allow them to bulk-add data to fill an entire graph.
- Displaying a visual connection between nodes
  - Nodes with related data should be visualised by a line between themselves and other relevant nodes.
- Highlighting links between selected nodes
  - When one has selected a node, it should focus this node in particular, and highlight its direct links more clearly.
- Scrolling and zooming through the graph(s)
  - The graph should be able to be scrolled and zoomed through, to allow for users to more easily see the connections.
- Switching between different view modes
  - The default grid alignments should have multiple options, so that data can be visualised in different ways to highlight different relationships (e.g. focusing on programme relationships or collaborator relationships).
- Searching for nodes via text input
  - A search box should consider nodes' titles, tags, and relationships, and highlight them accordingly when searching in the search box.
- Displaying details of project(s) for selected node
  - An overlay with details of the selected project should appear when a project node is selected.
- Scale to a mobile display
  - The site should maintain full functionality on a mobile device, without the need for a separate application / codebase.
- Scrape the university website to get staff details, particularly including research interests
  - Datasets should be able to be automatically filled by scraping the university website, in order to create a graph which can illustrate the connections between staff, in particular including their research interests.
- Simple user-data deployment pathway
  - Implementation of a connection with Google Sheets enabling users to import their own data and have it synchronise changes between the site and their personal storage.

##### Stretch Goals

- Version Control-based timeline
  - The site would provide the ability to step through the history of the data that has been saved in the site, to observe how the connections have changed over time.
- Virtual Reality Implementation
  - The graphs would be visible in a virtual reality element, providing the ability to interact in a much more dynamic and interesting fashion.

#### 1.3.3 User characteristics

**A VUW staff member(using our data):** Before visiting Connected Worlds this user is not guaranteed to have an
understanding of how to use the website or where they should look for information on how to use it. They are also not
guaranteed to understand the underlying components of the website. It is likely that they are familiar with using other
VUW portals and websites and should be familiar with uploading files and entering information into forms or sites.
Connected Worlds interface is slightly different to other VUW portals and sites so it is likely that this user may need
to be guided through the site, being taught how to use it along the way. This user may have a specific need when visiting
the site and their journey getting from their question to their answer should feel seamless and intuitive. This user may
also have no specific need when visiting the site and may just want to look around.

**A VUW staff member(with their own data):** This staff member is characterised in a similar way to a staff member
without data but has additional understanding about the data they have. It is unlikely however that they will understand
what format we need that data in without being guided through the format process or being told what the data requirements
are. It's likely that the staff member will be familiar with the `.csv` file format and will know how to use tools to
edit these files as required to correctly format their data.

**A non VUW staff member(using our data):** Similar to a VUW Staff Member, before visiting Connected Worlds this user is
not guaranteed to have an understanding of how to use the website or where they should look for information on how to use
it. Unlike a Staff Member it is unlikely that they are familiar with VUW websites and portals but are likely familiar
with non VUW websites and portals. This user will not have any understanding of the data they are seeing and will not
need to be able to upload or modify the data.

**A non VUW staff member(using their own data):** This user will have similar characteristics to the non VUW staff member
using our data but will have more knowledge about what data they have and how it is formatted. They aren't required to
understand what format the data needs to be in without being guided through the format process or being told what the
data requirements are. It's likely that the user will be familiar with the `.csv` file format and will know how to use
tools to edit these files as required to correctly format their data.

**A user familiar with VR/AR/MR:** This user will have an understanding of how to use Virtual Reality, Augmented Reality
or Mixed Reality. This user will be familiar with one of those interfaces and how to navigate virtual space. This user
may not have an understanding of how to use Connected Worlds but they should find the experience seamless and intuitive.
They may also not be familiar with data, or have any, so they should be able to use our data with very little effort.

**A user looking to extend Connected Worlds:** This user will either have a good understanding of websites and how they
function or will have gained a good understanding of how to use Connected Worlds. This user will have an entry level
understanding of programs and how they work and will be able to read the source code in the repository to form ideas
about how to extend Connected Worlds.

#### 1.3.4 Limitations

a) Regulatory policies  
 There may be regulations related to data usage and storage, as some data may be private. The product will also collect user interation metrics which requires persmission from the user. The admins of the product will also need to have passwords, which need to be protected and possibly encrypted.

b) Hardware Limitations  
 The process, assuming there are any, and the financial limitations of acquiring a
VR headset - if the product reaches a VR implementation.

c) Interfaces to other applications  
 The product does not require an interface to any other applications.

d) Parallel Operations  
 The gathering of user interaction metrics alongside the users interaction with the product.

e) Audit Functions  
 Requiring branches to be created off the master branch before any changes are made
to the product ensuring nothing malicious gets pushed. In this way, all commits are audited by atleast one other team member.

f) Control Functions  
 There are no control functions that may limit the product.

g) Higher-order Language Requirements  
 JavaScript knowledge, specifically the cytoscope.js library, may provide limitations
on what the team can produce.

h) Signal handshake protocols (e.g. XON-XOFF, ACK-NACK)  
 Signal handshake protocols are not applicable to this project.

i) Quality Requirements
Familiarity with digital design tools such as Unity, Adobe Illustrator, PhotoShop and GIMP, predominantly when it comes to the user interface optimisation for the mobile version. Experience and knowledge in terms of using the cytoscope.js library may limit the quality of what can be produced within the code of the product itself.

j) Criticality of the applications  
 Criticality is quite low - a working prototype already exists.

k) Safety and security considerations  
 Password protection for any admins of the product. As the product is hosted online password protection must be taken into consideration.
Data gathered through user metrics must also be kept private.

l) Physical/mental considerations  
 There are no physical or mental considerations applicable to the product.

---

## 2. References

IEEE Explore, Systems and software engineering — Life cycle processes — Requirements engineering,”
IEEE, 2022. [Online].
Available: http://www.ieeexplore.ieee.org/. [Accessed: Mar. 27, 2019].

Connectedworlds2.surge.sh. (2018).
Connected Worlds. [Online].
Available: https://connectedworlds2.surge.sh/ [Accessed 27 Mar. 2019].

"Cytoscape Swing App API (swing-app-api) 3.8.0-SNAPSHOT API",
cytoscape.org, 2019. [Online].
Available: http://code.cytoscape.org/jenkins/job/cytoscape-3-javadoc/javadoc/. [Accessed: 27 Mar. 2019].

---

<div style="page-break-after: always;"></div>

## 3. Specific requirements

### 3.1 External interfaces

#### Altmetric API

- The Altmetric API is the API for the Altmetric service. Altmetric provides data on how many times a paper was cited in order to describe how much your paper is talked about and the impact it has had in the community. Its purpose is to demonstrate the impact that a paper has had and where that impact was.
- Input is provided to the Altmetric API through HTTP GET requests in the URL. The format of these GET requests must follow Altmetrics guidelines which are kept up to date with their [api documentation](https://api.altmetric.com/). When makng a GET request to Altmetric you must provide your API key in the format `?key=xxx` at the end of the request url.
- Altmetric will only provide exact matches to GET requests. There are a range of [error codes](https://api.altmetric.com/) that the API may return and these will be checked in th first instance that anything goes wrong.
- Altmetric will respond to queries within one second and will accept no more than one query per second or 1.5 million queries in any rolling 30 day period.

#### ORCID API

- The ORCHD API is the API for the ORCID service. ORCID is a service that can be used to identify research and researchers, by providing a unique identifier for each.
- Input is provided to the ORCID API though HTTP GET requests in the url. The format of the GET requests must follow ORCIDs guidelines which are kept up to date on [their website](https://members.orcid.org/api/tutorial/read-orcid-records). ORCID iDs and access tokens are returned in JSON and API responses are returned with the following XML `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>`
- ORCID will only provide exact matches to GET requests. It will return a `404` if no ID is found for the provided query.
- ORCID will respond to queries within a maximum of 120 seconds. In most cases you can expect to get an [error code](https://members.orcid.org/api/resources/error-codes) well before the timeout period expires.

#### Local CSV Data

- The local csv data is the main relationship data that is used to construct the graph. It contains information about projects, staff members, students and their relationships.
- Currently, input is provided from a `.csv` file with the following format:

| name | role | faculty | school | programme | secondaryProgramme | email | collaborators | staffSiteLink | siteLink | siteName | bio | mediaLink |
| ---- | ---- | ------- | ------ | --------- | ------------------ | ----- | ------------- | ------------- | -------- | -------- | --- | --------- |


- This will be updated to a more generic set, where the user can define their field headers. The `name` field will act as a key value and be mandatory, but subsequent fields will be provided by the input data.

- As the data is stored locally it can be accessed immediately during execution and is stored in the program memory at the start of execution.

#### VUW Staff Profiles

- The VUW Staff profiles will be scraped in order to gather tags and descriptions on each staff member. The purpose of ths data is to give more insight int each of the staff members.
- Input will be provided from the staff website and then stored in a csv. This csv data will then be imported into Connected Worlds at the start of execution. The csv will have the following format:

| name | email | office | teaching | qualifications | interests |
| ---- | ----- | ------ | -------- | -------------- | --------- |


- As this data is stored locally it can be accessed immediately during execution. This can be re-scraped to ensure it is up-to-date, but does not need to be performed on each run.

### 3.2 Functions

1. Importing user data from a `.csv` file - **MVP** \
   _Enhancement_

   - Validity checks on input(s)
     - Data will need to be validity checked to ensure that it fits the required format to parsed correctly, and does not include any malicious input (e.g. SQL attacks).
   - Sequence of operations
     - User will upload a `.csv` file to the system.
     - System will check for any malicious inputs or incorrect formatting.
     - System will parse file into new objects and illustrate them on the graph.
   - Responses to abnormal situations
     - Overflow
       - To prevent overflow, file sizes will be checked and restricted based on the capabilities of the server.
     - Communication facilities
     - Error handling and recovery
       - Errors will be output to the user in an attempt to allow them to rectify these. In the event that it is unrecoverable, the page will reload to its previous state.
   - Effect of parameters
     - Larger files/datasets could impact the performance of the site.
   - Relationship of outputs to inputs
     - This use case will automatically involve the presentation use case, provided it executes successfully.

2. Displaying the graph to the user - **MVP** \
   _Enhancement_

   - Validity checks on input(s)
     - Will lock the display to be within the range of the nodes present, so that users cannot 'lose' the graph.
   - Sequence of operations
     - User will load their data.
     - Graph will be updated to illustrate the connections between the datasets.
   - Responses to abnormal situations
     - Error handling and recovery
       - Errors will be output to the user in an attempt to allow them to rectify these. In the event that it is unrecoverable, the page will reload to its previous state.
   - Relationship of outputs to inputs
     - The provided data is the most imporatnt decider in how the graph presents itself, and will involve a lot of logic to ensure it displays in a visually useful way.

3. Searching for nodes via text input - **MVP** \
   _Enhancement_

   - Validity checks on input(s)
     - Will need to prevent input of malicious code, as this will interact with the database and therefore could be prone to SQL attacks.
   - Sequence of operations
     - User will select the search box and begin typing a query.
     - System will operate a trie search through the nodes in the system.
     - System will list any nodes which hold data with the included string in it.
     - User can hover over results to highlight the node, or select a node to zoom into it.
   - Responses to abnormal situations
     - In the event disallowed characters try to be entered, they will be prevented at the text input stage, stopping them from reaching the code execution area.
   - Relationship of outputs to inputs
     - The resulting visual of the graph is entirely dependent on the node selected by the user. This can only be selected from a list that is output by the system, providing the relevant nodes to the query. Nodes provided in the search list will be found by their keywords and tags, along with their titles.

4. Displaying details of selected node - **MVP** \
   _Enhancement_

   - Sequence of operations
     - User will click a node
     - A popup will appear with relevant data, e.g. research interests for a `Member` node, or project details for a `Project` node.
     - This popup can be closed by the 'X' button or by clicking on an empty part of the graph, which will also deselect the node.
   - Responses to abnormal situations
     - In the event that the selected node has no details with it, the popup will still display a default value alongside its title to ensure the user does not think the program isn't functional.
   - Relationship of outputs to inputs
     - The popup contents is dependent on which node is selected. The presence of the popup itself is also dictated by where the user clicks on the screen.

5. Unique URLs per node \
   _New Feature_

   - Sequence of operations
     - User will load a dataset
       - (Optional) User will click a node and/or view mode
     - The system will generate a new URL and change the currently presented one to reflect this
   - Responses to abnormal situations
     - In the event of an 'invalid' link to a piece of data, the system will navigate up the 'chain' of the URL, stopping at whichever hierarchical level is lowest and provides a valid view. If this is not possible, it will otherwise default to the overall view.
   - Relationship of outputs to inputs
     - The URL will change each time a view mode is changed or a node is selected. This means the URL will be the ouput, and the node selected will be the input.
     - This will be reflexive, meaning that an input URL can also result in a change of visually selected node.

6. Date slider to view data history \
   _Enhancement_

   - Sequence of operations
     - User will load a dataset
     - The date slider will be adjusted for start or end date (or both).
     - Data will be filtered to only show values which existed within this date range, and their data at this time.
   - Responses to abnormal situations
     - In the event data cannot be retreived properly, the system will revert to the current date and display that it is doing so.
   - Relationship of outputs to inputs
     - The only data that will be shown to the user is that which is specified in the date range slider.

7. Adding a single node to the graph \
   _New Feature_

   - Validity checks on input(s)
     - Data will be input via a popup, which will enforce correct formatting before passing it to the backend.
   - Sequence of operations
     - User will select the 'add a node' button.
     - A popup will appear with labelled fields and description of required format.
     - Input will be parsed to ensure it is valid, and then a new node will be created.
     - The presentation method will be called with the updated dataset.
   - Responses to abnormal situations
     - Error handling and recovery
       - In the event that incorrectly formatted data is parsed beyond the popup's own checks, the page will reload to its previous state if an error occurs.
   - Relationship of outputs to inputs
     - This use case will automatically involve the presentation use case with updated inputs, provided it executes successfully.

8. Changing view mode \
   _Enhancement_

   - Sequence of operations
     - From the view mode, the user will select one of the alternative view methods.
     - The program will then operate on the same dataset as currently in use, but illustrating it in a different manner.
   - Responses to abnormal situations
     - Error handling and recovery
       - In the event that the data cannot be displayed in a different mode for some reason, it will revert to the previously used mode.

9. Scale to a mobile display \
   _Enhancement_

   - Sequence of operations
     - The user navigates to the site on a mobile device.
     - The site will recognise the display size, and move the overlay accordingly to ensure that all the essential components are visible.
   - Relationship of outputs to inputs
     - The site will function fundamentally the same as the desktop version. The only difference will be the location of overlays, which will be determined by the screen size identified by the program.

10. Scraping university site for member details \
    _New Feature_

- Validity checks on input(s)
  - Data will be pulled automatically from what is meant to be a consistent storage method. However, it will still require checks for null values and formatting styles, e.g. comma separated, line separated values, etc.
- Sequence of operations
  - The system will parse the website for staff details and store them temporarily.
  - The individual values will be checked for null or broken values
  - Provided inputs are valid, these will be passed into the database.
  - The system will then present these as normal.
- Responses to abnormal situations
  - In the event of a null value where it is not important, the value will be left blank.
  - In the event of a null value where the field is critical (e.g. name), the current user will be ignored, and an error message provided.
- Relationship of outputs to inputs
  - The parsing of data will change the graph significantly, so the display output at the end is dependent on the inputs being valid.

11. User tutorial \
    _New Feature_

- Sequence of operations
  - Upon first time visit to the site, a tutorial overlay will be presented.
  - The user can click through to learn how to use the site, or close the tutorial to dismiss it immediately.
  - The tutorial will provide illustrations highlighting how to use the various functionalities of the site.

### 3.3 Usability Requirements

Users of this system should be able to access the system through the internet using a web browser, and this should be possible both on a desktop, and on a mobile device. The process of accessing, and manuvering through the site must be simple enough for someone who may not be as competent using a web browser.

The site must run smoothly, with minimal delay between inputs, specifically when using the search function to search through data. Search should be possible using both keywords to search through 'people' nodes, project nodes, and programme nodes. The search must be efficient when dealing with a database of an arbitrary size.

There must be clear visual links between nodes depending on their relationships to each other, and these links should be easy to follow, without visual clutter. Users must be able to click on nodes to view more detailed information about the node.

Users accessing the site from a mobile device should have access to full functionality, including panning and zooming the display. This should be intuitive and should be consistent with minimal error or delay.

### 3.4 Performance requirements

The project is designed to be open sourced, and thus support a wide range of different datasets, alongside running on mobile devices.

- The website should load in a timely manner on any given device - it should take no more than 2 seconds to load the page, regardless of the size of the dataset (within reason). If this cannot be achieved, a loading display will be implemented to help encourage user patience.
- Scrolling and zooming through the dataset should be visually smooth.
- Switching between view layouts should take no longer than 0.1 seconds.

### 3.5 Logical database requirements

```plantuml
@startuml
interface Member
abstract class StaffMember
abstract class Student
Member <|.. StaffMember : implements
Member <|.. Student : implements
Programme "1" --- "*" Member
Project "*" --- "*" Member
Project "*" --- "*" Programme
Student <|.. MastersStudent : extends
Student <|.. PHDStudent : extends
StaffMember <|.. AcademicStaff : extends
StaffMember <|.. ProfessionalStaff : extends
Student <|.. HonoursStudent : extends
@enduml
```

The database needs to hold the relationships between each of these classes and illustrate the connection(s) between them. Each `Member` can be associated with multiple projects, but exactly one `Programme`. Each `Project` can have multiple `Programme`s associated with it, and each `Programme` can have multiple `Project`s.

The database's role is to host the values of these nodes and the connections between them, so that they can be illustrated whilst focusing on different super-classes (Programmes, Projects, or Members).

### 3.6 Design constraints

There are not many design constraints on the system itself - the project is designed to be open sourced, and operates on provided data by the user(s). It does not perform any important changes to provided data, rather just illustrates connections between them, and thus does not have any regulatory requirements or external standards to hold. The same holds for standards compliance - as no data changes are being made, instead only visualising the data provided, there are no particular needs for traceability or reporting through the program.

### 3.7 Nonfunctional Requirements

This project must be user friendly, as it needs to be both viewed, and manipulated by users.

It should be reliable, with smooth transitions between interfaces, and smooth panning and zooming.

It should be appealing for users to use, and no users should have difficulty in determining differences between colours for the various diagrams and interfaces. There should be minimal visual clutter where possible.

It should be safe for all users. There should be no rapidly flashing colours, or similar occurances to ensure that this system is not an epileptic hazard.

The system should be flexible, and should allow for all sorts of user, programme, and project nodes to be created. It should be flexible enough to have potential to be implemented in other environments outside of Victoria University and its partners.

It should be secure, and confidential information should either not be stored, or not be accessible to anyone other than the owner of that information.

It should be portable, and accessible from a mobile device, and not just larger desktop or laptop devices.

It should be maintainable. It should require minimal maintanence, but maintanence should be possible in the case of adding new features, or fixing broken features, without losing information that is stored. There should be frequent enough backups to ensure this.

### 3.8 Physical and Environmental Requirements

This system has no specific hardware components, besides the server that it is hosted on. This will be a Platform as a Service (PaaS) system, meaning that the performance requirements can be easily upscaled if necessary, and will be hosted in a server farm where reliability and environmental hazards can be managed easily.

The only requirements of this service will largely revolve around its Intellectual Property guidelines, as to ensure that the services hosted on the server will not be accessible to the party hosting it.

In the event that cloud hosting is not an option, a system with easily expandable components may be set up, which will be hosted in an environment where it is safe from external damage. This will help to ensure that in the event that the project expands to a large userbase, it will be possible to upgrade the hardware to support this without causing large amounts of downtime or extra expense.

### 3.9 Supporting information

Connected Worlds is a project that has come out of VUW pushing for inter-school projects. One of the many struggles that staff members find when trying to collaborate between schools is finding someone who has similar interests to them, or similar interests to the project they want to do. Currently the way to do this is to go through each staff members staff profile and read about them and their interests as the staff members themselves have described on their page. While this seems like it would work the consensus from staff members is that this is far to difficult to get any valuable information out of. The goal that Connected Worlds aims to solve is visualising this data in a way that is far more useful, navigable and easy to look at.

Connected Worlds will not need to be packaged by the end user, it will be deployed straight from the repository to a web server to be viewed by its users. If someone wishes to make changes to Connected Worlds they will need to clone the repository, run `npm install` and then host the static site with their own web server.

---

<div style="page-break-after: always;"></div>

### 4.0 Verification

The project will be approached with test-driven development. This will mean that tests will be created as a framework for new functions prior to their implementation, allowing for a methodological approach to defining requirements against functionality, and testing that said functionality is met.

These tests will also be implemented as part of the CI/CD pipeline, helping to ensure that it is much less likely for functionality-breaking changes to be pushed to a production build of the project, and providing greater confidence in the reliability of the product.

These tests will primarily use the native React testing functionality, but will also be backed up by some external libraries such as Enzyme.

Nonfunctional requirements will be verified through user testing, with monitored user tests to receive feedback on the product, and checking for user interface changes that may be needed based on how users interact with the prototyped product.

---

## 5. Development schedule.

### 5.1 Schedule

1. Architectural Prototype \
   _15/05/19_
2. Proof of Concept \
   _22/05/2019_
3. Further Releases \
   Sprint 6: _12/06/2019_ \
   Sprint 7: _26/06/2019_ \
   Sprint 8: _10/07/2019_ \
   Sprint 9: _24/07/2019_ \
   Sprint 10: _07/08/2019_ \
   Sprint 11: _21/08/2019_ \
   Sprint 12: _04/09/2019_ \
   Sprint 13: _18/09/2019_
4. Project Outcome \
   _11/10/2019_

### 5.2 Budget

| Item          | Cost               |
| ------------- | ------------------ |
| Gift Vouchers | $150               |
| Web Hosting   | $120               |
| ------------  | ------------------ |
| Total         | $270               |

Gift Vouchers:
We will require user feedback to ensure that our products nonfunctional requirements are adequately met. This may require surveying students or staff members. In order to incentivise participating in these tests, $15 gift vouchers may be offered to each participant. A sample group of 10 will provide us with adequate feedback, totalling around $150.

Web Hosting:
Currently, the website is being hosted for free under the free plan on surge.sh. Should the client wish for a more professional end product, the website can be hosted on Surge.sh for $30 per month. For the scope of this project, it would be hosted up to October 2019. At most, this would result in 4 months of hosting, up to $120. The site could alternatively be hosted on local ECS servers which may help to mitigate the necessity for this cost, but this will be subject to availability and the scope of the end-users of the project.

<div style="page-break-after: always;"></div>

### 5.3 Risks

| Risk                                                                                       | Likelihood | Impact | Mitigation Strategy                                                                                                                                                         |
| ------------------------------------------------------------------------------------------ | ---------- | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Requirements of the project change dramatically.                                           | Low        | Large  | Discuss with the client, and have clarity on exactly what it is that they require for this project.                                                                         |
| Meeting time slots between the project team, client and senior manager cannot be arranged. | Medium     | Medium | Ensure there is consistent, constant communication between all three parties involved so that we have as much time to organise meetings as possible.                        |
| Group members are temporarily unavailable to work on the project due to other commitments. | Medium     | Medium | Manage time sensibly and allow for adequete time to complete other commitments. Stay healthy and safe.                                                                      |
| Existing code base is horribly difficult to understand.                                    | Large      | Medium | Learn how to work with the language used, read design notes and the commit history of the code. \ Implement tests to protect against unexpected behaviour.                  |
| Client does not provide required resources in a timely manner.                             | Medium     | Large  | Explain to the client the importance of receiving access to these resources. Bring this up with the senior manager if required.                                             |
| The scope of the task is larger than we anticipate.                                        | Medium     | Medium | Frequently re-evaluate our predictions to ensure they are as accurate as possible.                                                                                          |
| The client continues to ask more and more of the group members.                            | Medium     | Medium | Kevin, our senior manager would attend all of our client meetings to help ensure we do not fall victim to scope creep.                                                      |
| We lose our work.                                                                          | Low        | Large  | Keep constant backups of our work, and previous revisions, whether it is through the GitLab, or on our own devices.                                                         |
| We do not have the skill set required to complete the task asked of us.                    | Low        | Large  | Learn the skills required, or discuss with the client and ask if what they wish for is what they really need to have as functionaility, and is it actually feasible for us. |
| We manage our time poorly.                                                                 | Medium     | Medium | Stay in frequent communication with other teammates so as to ensure no one falls behind, and help is provided when it is neccassary.                                        |

### 5.4 Health and Safety

Health and Safety considerations for this project are limited to occupational hazards such as excessive computer use and cable management.

In order to mitigate any risk of this, regular breaks will be taken throughout lab sessions, and any loose cables will be dealt with immediately upon notice.

Simple risks like open vessels around electronic devices will also be mitigated through disallowing these within the computer lab environment, as is already enforced.

#### 5.4.1 Safety Plans

Project requirements do not involve risk of death, serious harm, harm or injury.

---

<div style="page-break-after: always;"></div>

## 6. Appendices

### 6.1 Assumptions and dependencies

Connected Worlds relies on many dependencies. Should the dependencies drastically change it could
affect the requirements in the SRS. These dependencies are:

- **Web Server:** Connected Worlds will require a web server in order to make the website available via the internet. This could be a webhost such as Nginx, Apache or Lighttpd. While it is unlikely that the requirements for these services will change we will need to be aware of any changes that do occur because it will affect our ability to host the website.
- **Cytoscape:** Cytoscape is a fundamental part of Connected Worlds and is the only library used to visualise the data. If the library was to become unavailable or change we would have to make substantial changes to Connected Worlds and it would be highly likely that the requirements for Connected Worlds would change drastically.
- **GitLab CI:** CI/CD is an integral part of this project and relies heavily on the tools provided by the GitLab ecosystem. If for some reason GitLab was unavailable or a different git host was used then the CI/CD requirements would have to change as the features required have been tailored specifically to the GitLab tool suite.
- **NPM:** Connected Worlds uses NPM to manage it's packages and dependencies. NPM is updated regularly and so are the packages that it manages. It is essential that during the length of this products lifetime NPM is kept updated as well as all of the packages that it manages. If for some reason NPM became unavailable or changed drastically we would have to ensure that all of the packages are kept up to date manually. This would create additional requirements to the SRS.

### 6.2 Acronyms and abbreviations

- An **API** is a service, usually served over the internet, that allows you to access features or data of other systems,
  applications or operating systems.
- **AR or Augmented Reality** is a technology that superimposes computer generated images on a user's view
  of the real world
- **CD or Continuous Delivery** is the practice of keeping software life cycles short and ensuring software can be reliably released at any time.
- **CI or Continuous Integration** is the practice of keeping all development in a streamline and integrating development as often as possible to minimise integration issues.
- **CSV** is a comma separated file that can be used to store data. It is one of the simplest ways to store data
- **Cytoscape** is a Javascript library for visualising complex networks using attribute data.
- A **Database** is a structured set of data that is accessible in various ways
- **DevOps** is a set of practices that automates software development
- **GitLab** is a web based DevOps tool that provides a git repository manager with additional feature like a wiki and CI/CD tools.
- **MR or Mixed Reality** is the merging of computer generated worlds and the real world where objects and people
  can interact in real time with computer generated images.
- A **Node** is either a collection of information about a person, a school, or a project and can be related to other
  nodes
- **NPM or Node Package Manager** is a Javascript package manager. It consists of a client and a database of packages called the npm registry.
- **Open Source** is a license term that describes a project that can be extended, modified and distributed
- **Scraping** is a technique used to collect data from a website when an easy way to get access to the underlying data
  isn't provided
- **SRS or Software Requirements Specification** is a description of a software system that is being, or is going to be developed.
- **VR or Virtual Reality** is a computer generated environment that incorporates sensory auditory and visual feedback
- **VUW or Victoria University of Wellington** is a university in wellington where Connected Worlds is being developed.
- **Web Server** is an application that hosts web files and responds to web requests

## 7. Contributions

A one page statement of contributions that lists each member of the
group and what they contributed to this document.

- Geordie: 1.1, 1.2, 1.3.1, 1.3.3, 3.1, 3.9, 6.1, 6.2
- Alex: 1.3.2, 5.4, 3.4, 5.1, 3.5, 3.6, 3.8, 3.2
- Victor: 3.3, 3.7, 4.0, 5.3
- Raven: 1.3.4
