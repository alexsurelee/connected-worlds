# ENGR 301 Project _04_ Architectural Design and Proof-of-Concept

**Authors:** Alexander Lee, Raven Gotz-Tier, Victor Ong, Geordie Rogers, Angitha Ramesh

## 1. Introduction

### Client

**Matt Plummer** \
Information Technology Services \
matt.plummer@vuw.ac.nz

### 1.1 Purpose

Connected Worlds is an interactive online tool that can be used to visualise relationships and
pathways, collate digital resources and showcase research projects and tools. It offers users a digital journey that takes them from raw data to usable outputs in simple steps.

### 1.2 Scope

Connected Worlds is a web based application that visualises the relationships between people, projects, tools and groups.
It will be able to run on both mobile and desktop devices and will function as an ecosystem that can be used to view
relationships between different elements for a wide range of use cases. During this project we aim to complete the
following goals which are expanded on throughout this document:

- Strengthen the existing prototype with testing and continuous integration
- Optimise the site for mobile users
- Extend Connected Worlds to offer a guided journey that takes users from raw data to usable outcomes
- Expand the data available by harnessing additional data from APIs

Anyone will be able to use Connected Worlds to visualise their connected data to get meaningful insights.

<div style="page-break-after: always;"></div>

### 1.3 Changes to requirements

Two more requirements were added to the project at the request of the client. These have also been added to the requirements document and are:

- Simple user-data deployment pathway
- Implementation of a connection with Google Sheets enabling users to import their own data

The way that CSV data is stored has been changed to accommodate these new requirements. They now have less requirements for structure and only require a few key elements such as name and tags.

The features that would be implemented at each stage of the project were refined and split up to into the sprints.

The performance requirements have been updated as follows:

> It should take no more than 2 seconds to load the page, regardless of the size of the dataset (within reason). If this cannot be achieved, a loading display will be implemented to help encourage user patience

Verification was extended to include CI/CD pipelines to ensure performance could be verified with metrics.

## 2. References

Connectedworlds2.surge.sh. (2018).
Connected Worlds. [Online].
Available: https://connectedworlds2.surge.sh/ [Accessed 27 Mar. 2019].

"Cytoscape Swing App API (swing-app-api) 3.8.0-SNAPSHOT API",
cytoscape.org, 2019. [Online].
Available: http://code.cytoscape.org/jenkins/job/cytoscape-3-javadoc/javadoc/. [Accessed: 27 Mar. 2019].

"React Top Level API",
React, 2019 [Online].
Available: https://reactjs.org/docs/react-api.html [Accessed: 14 May. 2019]

"The Falcon Web Framework",
Flacon, 2019. [Online].
Available: https://falcon.readthedocs.io/en/latest/ [Accessed: 14 May. 2019]

"Google Sheets API"
Google, 2019. [Online].
Available: https://developers.google.com/sheets/api/ [Accessed: 14 May. 2019]

<div style="page-break-after: always;"></div>

## 3. Architecture

### 3.1 Stakeholders

**Stakeholder Register:**  

|Name|Position|Internal/External|Project Role|Contact Information|
|----|--------|-----------------|------------|-------------------|
|Matt Plummer    |ITS Contact       |Internal                 |Client            |matt.plummer@vuw.ac.nz                   |
|Information Technology Services|Service|Internal|Client|its-service@vuw.ac.nz|
|Users|All|Internal/External|Customer|N/A|

**Stakeholder Concerns:**  

|Concern|Stakeholders|
|----|-------|
|Users are able to access the site. The product is no longer valuable if it cannot be accessed|Matt Plummer, Information Technology Services, Users|
|If the site is not easy to use or requires too many instructions it will deter users |Matt Plummer, Users|
|The financial cost of hosting the site|Matt Plummer, Information Technology Services|
|A product can be delivered before the end of the project|Matt Plummer, Information Technology Services|
|The site becomes hard to maintain |Matt Plummer, Information Technology Services|
|The site becomes hard to extend|Matt Plummer, Information Technology Services|
|The external organisations that are depended on become unavailable (Google Sheets)|Matt Plummer, Information Technology Services, Users|

### 3.2 Architectural Viewpoints

**Logical:** Describes the overview of objects within the system and their interactions between one another. This scopes the digital structure of the application, supporting the functional requirements of the project by mapping abstractions of the requirements to objects. The logical view frames the following architectural concerns:

- Users are able to access the site \
    Users not being able to access the site due to incompatible software, hosting issues, or crashing software are critical concerns for the project.
- The cost of hosting the site \
    The site should not cost more to host than the value it provides, as this may result in it needing to be shut down.
- The site is easy to maintain \
    The site should not be implemented in an excessively complicated way, else it will be harder to fix any issues that may occur or need to be fixed.
- The site is easy to extend \
    The site's structure should be easily understood, so that it may be expanded as a project in the future.

**Development:** Describes the organisation of the software modules within the development environment, i.e. subsystems and their hierarchical layers, defining the interfacing between them. This scopes the map between individual requirements and tasks for the developers. The development view frames the following architectural concerns:

- A product can be delivered before the end of the project \
    If the product is not finalised completely for the end of the project, it could result in becoming very difficult to maintain and expand on, as sections left in an incomplete state are difficult to build upon.

**Process:** Describes the non-technical / non-functional requirements that must still be met through the development process, e.g. performance, system availability, fault-tolerance, etc. The process view frames the following architectural concerns:

- The learning curve for users \
    If the site's usability is not clear to users, it will be difficult to expand usage and make it a valuable piece of software.
- The site responds to user input intuitively \
    If the site does not conform to user expectations, it can create friction against their experience.

**Physical:** Describes the system's physical functional requirements that must be met by the product, e.g. availability, reliability, performance, scalability, etc. Maps the elements within the logical, process, and development views to these requirements. The physical view frames the following architectural concerns:

- Users are able to access the site \
    Users not being able to access the site due to incompatible software, hosting issues, or crashing software are critical concerns for the project.
- The cost of hosting the site \
    The site should not cost more to host than the value it provides, as this may result in it needing to be shut down.

**Scenarios:** Describes the links between the other four viewpoints via specific use case examples, illustrating an abstraction of the primary requirements of the project against its actual functionality.

<div style="page-break-after: always;"></div>

### 4. Architectural Views

### 4.1 Logical

#### Packages

Connected Worlds is separated into multiple packages to make it easier to maintain.

* The `frontend package` is responsible for handling the view and interactions with the User
* The `components package` holds visible elements that can be rendered
* The `util package` has elements that are used for computation and storing the data model
* The `layout package` stores the possible layouts for Cytoscape to use
* The `backend package` is responsible for retrieving and parsing the data for the `frontend package`

![package-diagram.jpg](package-diagram.jpg)

<div style="page-break-after: always"></div>

#### Classes

**Back End**

The `back end package` has four classes in it. The main interfacing class is the API which is an implementation of the falcon API framework. This allows HTTP calls to be made to the API and be processed by the other CSV scripts. The Sheets Handler is the other interfacing class that is an extension on top of the sheets API. This allows us to call pre-constructed functions that do most of the authentication and call handling for us. The two other classes are logic classes and are responsible for actually formatting the data and validating it before it is passed back to the API to be returned.

![backend-class.jpg](backend-class.jpg)

<div style="page-break-after: always"></div>

**Front End - Components**

The `components package` contain all of the visible components for the front end. Most of the components in the components class extend the React.Component which implements the actually rendering of the object and data handling. The other components (`StyleCytoscape`, `StylePage`, `SearchItem` and `SearchResults`) are all components that handle computation and don't present any view to the user and instead provide computation and data for the other components which then render themselves for the user.

![components-class.jpg](components-class.jpg)

<div style="page-break-after: always"></div>

**Front End - Layouts**

The `layouts package` consists of five classes. Layout is the main class of the package, it is an abstract class and all of the other classes extend it. Layout provides the basic requirements for formatting the cytoscape view and each layout that implements that class then specifies how that view is changed and how nodes are related to create a new Layout.

![layouts-class.jpg](layouts-class.jpg)

<div style="page-break-after: always"></div>

**Front End - Util**

The `util package` is a computational package that is used by other packages to perform the most common operations. CytoscapeStore handles all of the complex computation and data store for the main Cytoscape class which handles the rendering and view. DataFilter and LayoutFactory are both used for creating valid data structures and layouts to be used by other front end classes. NetworkRequest handles the interfacing with the back end and retrieves data.

![util-class.jpg](util-class.jpg)

### 4.2 Development
The Connected Worlds project is subdivided into a small number of core software modules with a hierarchical approach, although hosts a fairly 'short' structure - individual components are much closer to the 'ceiling' of the project, ensuring there is less room for unintended flow-on effects of changes, as they do not need to be traced through a lot of layers within the system. Unfortunately, the project is not properly divided into a Model-View-Controller view, meaning that multiple developers working on the related areas simultaneously can result in more time required for resolution of conflicts. Ideally this should be refactored, but is not the focus for this project.

| Connected Worlds |                                                                 |
| ---------------- | --------------------------------------------------------------- |
| Layer 5          | User Interface & External Connections                           |
| Layer 4          | Implementation and adjustment to Cytoscape and React frameworks |
| Layer 3          | Cytoscape Library                                               |
| Layer 2          | React Framework                                                 |
| Layer 1          | Javascript / HTML implementations                               |

**Layer 1** surrounds the implementation of the basic Javascript and HTML of the project, which should operate as a domain-independent infrastructure upon which the project can be built to operate on a multitude of devices.

**Layer 2** incorporates the React framework which is utilised throughout the project, and provides much structure to the logical view. It should also be fairly domain-independent, as it is present on many modern websites for both web and mobile.

**Layer 3** integrates the Cytoscape library itself as domain-specific architecture, performing most of the 'heavy-lifting' of the project and enabling the bulk of the app to operate. It is common throughout the entire project.

**Layer 4** then further develops upon the previous layers, implementing more project-specific functionality on top of that which is provided by the lower 3 layers. This is represented by the `util package` and some of the `components package`.

**Layer 5** contains the bulk of the user interface itself, and outlines the direct interactions between the user and the product. This also includes the external integrations to the project, such as the use of external APIs, as these operate independently of the project itself. This layer is user-dependent, as it allows for customisation of view options and data inclusions on a per-user basis - it is largely represented by the `layouts package`, and some of the `back end package`.

<div style="page-break-after: always"></div>

### 4.3 Process

The first interaction that the user will have with the product is navigating to the site. The diagram below goes over the possible paths that the program can take during this process. The first check that is made is wether we support the users browser or not. Users will be given the option to continue using the site or to exit. The most important things to note is that any HTTP status code other than `200` received from Google Sheets will result in an error and the data not being displayed correctly. We do this so that we can catch any possible errors as soon as possible at the source of the problem. There are also multiple steps in the back end that can cause an error and these are caught as soon as possible as well so we can display that error to the user. These errors are passed back to the front end with a `5XX` Status code as well as a message describing the error. Displaying the data can also fail and hence will return an error to the user.

![load-process.jpg](load-process.jpg)

The second process is getting the required data and this is completed by the back end. The back end first needs to decide if the data we want to retrieve is example data that is public to everyone or user specific data that requires authentication to retrieve. If the data the front end wants is example data then no authentication is required and th request can be sent directly. If the data is user specific then the back end will have to generate a token for the user and then send that token along with the request. The data is then taken by the backend and passed back to the front end.

![data-process.jpg](data-process.jpg)

<div style="page-break-after: always"></div>

The main interaction is when the user is using the main functions of the product. These interactions can be considered the main interactions between the product and the user. There are two main interactions that the user can make, the first is a user hovering their mouse over a node. When a user hovers over the node Connected Worlds will highlight all of the connected nodes. If the user has enabled the details view, which is enabled by default, then clicking on the node will bring up the details pane for that node, if clicking is disabled then nothing more will happen and the application will wait for the next interaction. The other interaction that the user can perform is changing the view. Changing this view leads to a change in the underlying data model, hence the data model must be updated before updating the view. Once the view is updated the application then goes back into the waiting state.

![interaction-process.jpg](interaction-process.jpg)

<div style="page-break-after: always;"></div>

### 4.4 Physical

The requirements of this product describe that Connected Worlds is able to handle interfacing with other services such as Google Sheets. This is reflected in the architecture of the system by separating the front end and back end into separate components. While these components are separate they will be executed on the same computing node and will communicate with each other locally over the HTTP protocol. The front end consists of multiple components which are all run on the clients hardware sourced from the central server. That same server will also store the components for the back end but will be responsible for executing back end operations.


### 4.5 Scenarios

**Scenario 1: Visualising the Default Data**
The below diagram shows a visual representation of the following scenario:

1. The user visits connected worlds from a supported web browser
2. The front end displays a loading animation to the user
3. The front end requests data from back end
4. The back end returns data
5. The front end renders the data to the user

![](scenario-default.jpg)

<div style="page-break-after: always"></div>

**Scenario 2: Visualising Custom Data**
The below diagram shows a visual representation of the following scenario:

1. The user creates a valid dataset in their own google sheet
2. The user visits connected worlds from a supported web browser and requests to view their own data
3. The front end requests a sheet token from the user
4. User requests the sheet token through the google sheets web interface
5. Google sheets returns the token to the user
6. The user enters the sheet token
7. The front end forwards the token onto the back end
8. The back end requests the data from the google sheet using the google sheet token
9. The google sheets API returns the data to the back end
10. The back end returns the formatted and verified data to the front end
11. The front end renders the data for the user to see and interact with

![](scenario-custom.jpg)
<div style="page-break-after: always;"></div>

## 5. Development Schedule

### 5.1 Schedule
![](schedule.png)

### 5.2 Budget and Procurement

#### 5.2.1 Budget

| Item | Cost | Date Incurred | Count | **Total** |
| --- | --- | ---| ---| ---|
|Gift Vouchers | $15 per person | In Trimester 2 | 20 people| **$300** 
|Web Hosting| $25 per month | In Trimester 1 and 2 | 6 months |**$150**
|**Total** | | | |**$450**

**Gift Vouchers:**\
Gift vouchers are to incentivise people for when we reach the user testing stage of our product.
We do not believe that the gift vouchers will be used during the first half of the project (ENGR301) because
we wish to use the vouchers for when our mobile app and possibly even the VR application of the project
is available for testing. Our main focus for this half of the year is the website and optimising the mobile application.
Previously we allocated $10 to $20 per person but we have agreed that $15  is an appropriate value to gift each participant. 
We aim to gather 20 people to test our product and that is how our total for the gift vouchers comes to a total of 
$300. Should the circumstances change, and we require more people for testing, we will divide the $300 as appropriately as we can 
between the number of participants. Should we manage to test fewer than 20 people, then we will retain the $15 value per person, and
we will incur a smaller cost rather than dividing the $300 between the people. 

**Web Hosting:**\
We will require a website hosting service other than Surge or GitLab Pages which is what our project
is utilising at the moment. Our project integrates a more complex application structure with 
a Node front end and Python back end, which is why a service like GitLab pages
is no longer a viable option. Since we need a minimum viable product by the end of this course, it 
is likely that we may incur this cost before Trimester 2. Since we have 6 months left until the end of trimester 2, the maximum 
cost we may incur for web hosting should only be for 6 months. After the 6 months, when our project has been completed, Matt Plummer has agreed to take over 
paying for the web hosting. The services we have been investigating include Code Deploy, Aerobatics,
BitBucket and Heroku. The cheapest of the options is from BitBucket at $0 per month. However, upon further investigation the service BitBucket provides 
is similar to GitHub Pages. From our research Code Deploy is also free of charge while Aerobatics and Heroku are charged on a monthly basis 
at $15 and $25 respectively. Should Code Deploy not be a viable option, we would need to use a charged service, preferably Aerobatics at $15 per month. 
For this budget however, since we have not made a decision yet, we have allocated the maximum cost that may be incurred which is $25 per month with Heroku.
To clarify, $150 is the maximum cost we may incur and does not accurately reflect the costs of web hosting that may actually be incurred due to both the
number of months we use the service and which service we decide on.


#### 5.2.2 Procurement

| Name | Good or Service | Source | 
|---|---|---|
|Cytoscape | Goods | Open Source |
|Web Hosting | Service | External Organisation |
|User Testing | Service | Victoria University Student and Staff members
|React | Service | External Organisation / Open Source
|Falcon | Service | Open Source 
|Google Sheets API | Goods | External Organisation

**Cytoscape:** \
Cytoscape is a library that we will be incorporating into our project in order to visualise and analyse the data we have at hand as a network. 
Our client Matt Plummer has suggested we implement or at least use the products of Cytoscape as a reference when we improve the existing web and mobile applications. 
He has expressed that the way Cytoscape allows visualisation of the data is what he wants us to maintain in our project and so this library is important in the deployment of our product, both the 
minimal and final. As a library this would be categorized as a good and as it is open source, we will be sourcing it from the internet. 

**Web Hosting:** \
Web hosting is a service from an external organisation that we will need to invest in later in the course of our project, for our website. It will be done through an external organisation such as Code Deploy, Aerobatics, BitBucket or Heroku. 
We have not decided on an organisation as our needs may change and it is not of high priority at the moment. Nearing the date of our presentation we will decide on an organisation with the research 
we have gathered on the service that best suits our needs at the time. 

**User Testing:** \
User testing will be vital to the quality of our final product because our project is heavily dependent of the user interface and notion of presenting data in the most accessible and aesthetic manner.
We have allocated $300 in our budget towards user testing to go towards gift vouchers to thank the participants for their feedback and time as well as provide incentive. Without user testing we may 
finish our project unaware of the quality of our project and how the client will receive it. With user testing we will be able to predict and solve problems within our project that we may have overlooked otherwise.

**React:**\
React is a javascript library that is used for building user interfaces. This is used in the front-end of the project for creating components like buttons and nodes, on our webpage and mobile application.
We require this library in order to create one of the most important aspects of our website and mobile apps - the components. We have several components, most of which are simply buttons and nodes. 
By nodes we mean, the icons with a project or staff member's name. The nodes will be assembled together using the Cytoscape library to visualise the data into a network.

**Falcon:**\
Falcon is used for creating APIs efficiently. This is an open source framework. The service will aid us in the interaction between components on our page and mobile app. 
Instead of creating an API from scratch, Falcon provides a framework that we can 'fill-in-the-blanks' for to suit the project. It is vital to our project as it saves time and will be easier to manage
and adjust as we change and rearrange components on our website and app.

**Google Sheets API:**\
We will be using google sheets in order to store the data that we will be translating into a visual network. Information such as staff members, project names, project descriptions and students, to name a few
will be stored in the sheets. We switched to google sheets API in order to satisfy our client's request of providing the feature to personalise the network with a user's own data. By enforcing google 
sheets we can provide users with a link to the database through which the user can add, remove or update to satisfy their own needs. 
<div style="page-break-after: always;"></div>

### 5.3 Risks

| Risk | Likelihood | Impact | Mitigation Strategy |
|------|------------|--------|---------------------|
| Requirements of the project change dramatically. | Low | Large | Discuss with the client, and have clarity on exactly what it is that they require for this project.
| Meeting time slots between the project team, client and senior manager cannot be arranged. | Medium | Medium | Ensure there is consistent, constant communication between all three parties involved so that we have as much time to organise meetings as possible.
| Group members are temporarily unavailable to work on the project due to other commitments. | Medium | Medium | Manage time sensibly and allow for adequete time to complete other commitments. Stay healthy and safe.
| Existing code base is horribly difficult to understand. | Large | Medium | Learn how to work with the language used, read design notes and the commit history of the code. Write tests to determine code functionality.
| Client does not provide required resources, (feedback, previous code), in a timely manner. | Medium | Large | Explain to the client the importance of receiving access to these resources. Bring this up with the senior manager if required.
| The scope of the task is larger than we anticipate. | Medium | Medium | Frequently evaluate the progress of our tasks, and ensure that all tasks are on track to be completed. Evaluate the importance of the next set of tasks as we transition between sprints.
| The client continues to ask more and more of the group members. | Medium | Medium | Keep a clear goal for an end product in mind, so that if neccassary, we can remind the client that more addtions may be outside of the scope of the project.
| The client is unsatisfied with our progress and abandons the project. | Low | Large | Ensure that work is kept on track, and that the client is constantly given updates on our progress. This is to reassure the client that we are on track to perform the task they wish us to perform.
| We lose our work. | Low | Large | Keep constant backups of our work, and previous revisions, whether it is through the GitLab, or on our own devices.
| We do not have the skill set required to complete the task asked of us. | Low | Large | Learn the skills required, or discuss with the client and ask if what they wish for is what they really need to have as functionaility, and is it actually feasible for us.
| We manage our time poorly. | Medium | Medium | Plan ahead and set specific deadlines for each task. Evaluate the progess of each task as the deadlines near and take appropriate measures to ensure that deadlines are met.

### 5.4 Health and Safety

There are very few health and safety considerations involved with this project. These are limited to occupational hazards such as excessive computer use, seating posture, and cable management.

In order to mitigate these risks, regular breaks would be taken during the lab sessions so as to avoid computer overuse. A relaxed work atmosphere will be encouraged so as to ensure all work is performed comfortably. Any loose cables, or other possible physical hazards near the workspaces will be dealt with immediately.

Other simple risks such as opened vessels around electronics will also be mitigated through disallowing these within the computer lab environment, as these risks are allready enforced by the Victoria University Engineering and Computer Science lab rules.

#### 5.4.1 Safety Plans

The project requirements do not induce a risk of death, serious harm, or injury, therefore there will be no safety plans implemented that differ to daily life.
<div style="page-break-after: always;"></div>

## 6. Appendices

### 6.1 Assumptions and dependencies

Connected Worlds relies on many dependencies. Should the dependencies drastically change it could
affect the requirements in the SRS. These dependencies are:

- **Web Server:** Connected Worlds will require a web server in order to make the website available via the internet. This could be a webhost such as Nginx, Apache or Lighttpd. While it is unlikely that the requirements for these services will change we will need to be aware of any changes that do occur because it will affect our ability to host the website.
- **Cytoscape:** Cytoscape is a fundamental part of Connected Worlds and is the only library used to visualise the data. If the library was to become unavailable or change we would have to make substantial changes to Connected Worlds and it would be highly likely that the requirements for Connected Worlds would change drastically.
- **GitLab CI:** CI/CD is an integral part of this project and relies heavily on the tools provided by the GitLab ecosystem. If for some reason GitLab was unavailable or a different git host was used then the CI/CD requirements would have to change as the features required have been tailored specifically to the GitLab tool suite.
- **NPM:** Connected Worlds uses NPM to manage it's packages and dependencies. NPM is updated regularly and so are the packages that it manages. It is essential that during the length of this products lifetime NPM is kept updated as well as all of the packages that it manages. If for some reason NPM became unavailable or changed drastically we would have to ensure that all of the packages are kept up to date manually. This would create additional requirements to the SRS.
- **React:** React is the base for the entire front end of our product. React is sponsored by multiple large companies to keep it alive but if for some reason funding were to stop React may no longer be maintained. If this were to happen our site would still work but it would be unlikely that security exploits would be fixed.
- **Falcon API:** Falcon forms a fundamental part of our back end that allows us to interface with other back ends. If for some reason falcon was no longer supported we could look at switching which library we use on the back end. This would only have a small effect on the project because most frameworks are built in a similar fashion so refactoring the code should take only a short amount of time.
- **Google Sheets:** Google Sheets is a core part of our project. It is the only place that data will be stored so if it were to become unavailable we wouldn't have a place to store this data. This project is being extended specifically to support Google Sheets so if it does become unavailable we would need to make significant changes to the requirements and the site itself.

### 6.2 Acronyms and abbreviations

- An **API** is a service, usually served over the internet, that allows you to access features or data of other systems,
  applications or operating systems.
- **AR or Augmented Reality** is a technology that superimposes computer generated images on a user's view
  of the real world
- **CD or Continuous Delivery** is the practice of keeping software life cycles short and ensuring software can be reliably released at any time.
- **CI or Continuous Integration** is the practice of keeping all development in a streamline and integrating development as often as possible to minimise integration issues.
- **CSV** is a comma separated file that can be used to store data. It is one of the simplest ways to store data
- **Cytoscape** is a Javascript library for visualising complex networks using attribute data.
- A **Database** is a structured set of data that is accessible in various ways
- **DevOps** is a set of practices that automates software development
- **GitLab** is a web based DevOps tool that provides a git repository manager with additional feature like a wiki and CI/CD tools.
- **Google Sheets** is a service that allows you to publicly share spreadsheets
- **MR or Mixed Reality** is the merging of computer generated worlds and the real world where objects and people
  can interact in real time with computer generated images.
- A **Node** is either a collection of information about a person, a school, or a project and can be related to other
  nodes
- **NPM or Node Package Manager** is a Javascript package manager. It consists of a client and a database of packages called the npm registry.
- **Open Source** is a license term that describes a project that can be extended, modified and distributed
- **React** is a front end javascript framework for building websites
- **Scraping** is a technique used to collect data from a website when an easy way to get access to the underlying data
  isn't provided
- **SRS or Software Requirements Specification** is a description of a software system that is being, or is going to be developed.
- **VR or Virtual Reality** is a computer generated environment that incorporates sensory auditory and visual feedback
- **VUW or Victoria University of Wellington** is a university in wellington where Connected Worlds is being developed.
- **Web Server** is an application that hosts web files and responds to web requests

## 7. Contributions

- Alex: 1.3 (Changes), 3.2, 4.2, 5.1
- Angitha: 3.1 (Part), 5.2.1, 5.2.2
- Geordie: 1.1, 1.2, 1.3 (Summary), 3.1 (Part), 3.2 (Part), 4.1, 4.3, 4.4, 4.5, 5.1 (Part), 6.1, 6.2
- Raven: 3.1, 4.5
- Victor: 5.3, 5.4, 5.4.1
