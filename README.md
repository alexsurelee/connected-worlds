# Connected Worlds

Connected Worlds is an interactive online tool that can be used to visualise relationships and pathways, collate digital resources and showcase research projects and tools. It offers users a digital journey that takes users from raw data to usable outputs in simple steps.

## Documentation

If you want to read up on how Connected Worlds is developed and how it works check out our [wiki](https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/wikis/home)

## Install

Connected worlds is build using React for the front end and python for the back end. The front end can be developed separately from the back end as it will use backup data if it can't connect to the front end.

**Prerequisites:** [Node](https://nodejs.org/en/), [Python3](https://www.python.org/), [Falcon](https://falconframework.org/), [Sheets v4](https://developers.google.com/sheets/api/quickstart/python), [Gunicorn](https://gunicorn.org/)

1. To get started clone a copy of this repository using `git clone git@gitlab.ecs.vuw.ac.nz:ENGR300-2019/Project-04/connected-worlds.git`
2. Enter the `connected-worlds` directory and run `npm install` to install all of the required dependencies
3. Start the back end using a `WSGI` like `gunicorn`. CD into `back-end/` and run `gunicorn api:app`
4. Start the front end. CD to the root directory of `connected-worlds` and run `npm start`
5. You're now in development mode, the front nd will automatically reload when you make changes

## Contributing

Connected Worlds is developed under a GNU General Public License. 

### Getting Started

If you're looking for something do do you can find current issues that need to be worked on [here](https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/issues). If you have an idea and what to work on it create a ticket [here](https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/issues/new) and let everyone know what you're working on

### Keeping Work Visible

While you are working on your ticket make sure to push your changes often so that if you decide you no longer want to work on the issue someone can pick up from where you left off.

### Merging Your Work

Once you've finished what you're working on make a merge request. This allows everyone to see what you've done and have a look at it. Don't worry it's not as scary as it sounds. Once everyone has looked at your awesome work and they like what you've done they will merge it into production. This is a place where everyone can test out your new features and changes before they get merged into the master branch as part of a release.
