# CI

## Image Files

This folder contains `.image.json` files that can be used to build the respective docker images. To build a docker image
you will need to install [packer from here](https://www.packer.io/downloads.html)

You will then be able to build the images with `packer build ci/back-end.image.json`

Once the docker image has been built you will need to upload it to the container registry by following the 
instructions [here](https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/container_registry)
