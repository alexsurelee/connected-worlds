import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import App from "./App";
import "./HomePage.css";
import BrowserDetection from "./components/BrowserDetection";
import GooglePicker from "react-google-picker";

class HomePage extends Component {
  render() {
    return (
      <Router>
        <div>
          <nav className="router">
            <Link to="/">
              <div className="redirect">Home</div>
            </Link>
            <Link to="/app/">
              <div className="redirect">App</div>
            </Link>
          </nav>
          <Route exact path="/" component={Home} />
          <Route path="/app/" component={App} />
        </div>
      </Router>
    );
  }
}

function Home() {
  return (
    <div>
      <div className="text title">CONNECTED WORLDS</div>
      <div className="text subheader">Welcome to Connected Worlds</div>
      <p className="text intro">
        Connected Worlds is a network visualisation webapp which enables you to
        illustrate and navigate connections between data in an interactive way.{" "}
      </p>
      <p className="text intro">
        To get started, create a copy of the template sheet below. You can fill
        your information into this sheet as you please. Currently, the headers
        are non-adjustable.
      </p>
      <p className="text intro">
        If you've already done this, simply choose your sheet to get back into
        it.
      </p>
      <br />
      <br />
      <p className="text intro">
        View this markdown file for a formatting guideline (link needs to be
        added once the project is open sourced)
      </p>
      <p className="text intro">
        https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/blob/2018-code-archive/docs/documentation/guides/data-uploading-and-formatting.md
      </p>
      <br />
      <br />

      <div className="button-container">
        <GooglePicker
          clientId={
            "30437934706-smjstag8pkarm77v876rv8qtb8o92j1t.apps.googleusercontent.com"
          }
          developerKey={"AIzaSyAZeK-zobcispG1aZLjlXdQ9NoisoDJiS8"}
          scope={["https://www.googleapis.com/auth/drive.readonly"]}
          // onChange={data => console.log('on change:', data)}
          onChange={data => {
            data.action === "picked" &&
              (window.location.href =
                window.location.href + `app/${data.docs[0].id}`);
          }}
          onAuthenticate={token => console.log("oauth token:", token)}
          onAuthFailed={data => console.log("on auth failed:", data)}
          multiselect={true}
          navHidden={true}
          authImmediate={false}
          mimeTypes={["application/vnd.google-apps.spreadsheet"]}
          viewId={"DOCS"}
        >
          <div className="button">Choose a Sheet</div>
        </GooglePicker>
        <a
          className="button"
          href="https://docs.google.com/spreadsheets/d/1cMFxSzIu7Dg_Xd8SyjsObReMAmXd13p9M-y75lX5nVA/copy"
        >
          New Sheet
        </a>
      </div>
      <BrowserDetection />
    </div>
  );
}

export default HomePage;
