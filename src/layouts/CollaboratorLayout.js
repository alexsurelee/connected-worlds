import Layout from "./Layout";

class CollaboratorLayout extends Layout {
  static getLayout() {
    this.cy
      .elements()
      .selectify()
      .grabify();

    this.clearStyles();
    const specialFilter = this.specialTypes
      .map(type => `[type = '${type}']`)
      .join(",");
    let elesFilter = this.cy.elements(specialFilter);

    const activePeople = this.cy.edges('[type = "collab"]').connectedNodes();
    const nonActivePeople = this.cy
      .nodes('[type = "person"]')
      .not(activePeople);
    elesFilter = elesFilter.add(nonActivePeople);

    elesFilter.addClass("filtered");
    const layout = activePeople.layout({
      name: "circle",
      avoidOverlap: false,
      padding: this.layoutPadding,
      radius: this.circleRadius(activePeople),
      nodeDimensionsIncludeLabels: false,
      sort: this.sortBy("reverse")
    });

    layout.run();

    this.cy
      .nodes()
      .not(activePeople)
      .position({
        x: this.cy.width() / 2,
        y: this.cy.height() / 2
      });

    return [layout];
  }
}

export default CollaboratorLayout;
