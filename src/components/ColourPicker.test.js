import React from "react";
import ReactDOM from "react-dom";
import ColourPicker from "./ColourPicker";
import { shallow } from "enzyme";

describe("<ColourPicker/> Tests", () => {
  it("1. Renders ColourPicker without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<ColourPicker />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders ColourPicker correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<ColourPicker />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });
});
