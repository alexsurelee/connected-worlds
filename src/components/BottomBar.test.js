import React from "react";
import ReactDOM from "react-dom";
import BottomBar from "./BottomBar";
import Search from "./Search";
import DataSwitch from "./DataSwitch";
import HelpButton from "./HelpButton";
import cytoscapeStore from "../util/CytoscapeStore";
import { shallow, mount } from "enzyme";

describe("<BottomBar/> Tests", () => {
  it("1. renders bottom bar without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<BottomBar />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. renders bottom bar correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<BottomBar />, div); //div is container for bottomBar
    expect(div).toMatchSnapshot(); // enzyme assertion
    ReactDOM.unmountComponentAtNode(div);
  });

  // create an universal wrapper for BottomBar
  cytoscapeStore.loadingFinished = true;
  const wrapper = shallow(<BottomBar />);

  it("3. Outer div contains 9 children elements", () => {
    const wrapper1 = shallow(<BottomBar />);
    expect(wrapper1.find("div").children().length).toBe(9);
  });

  it("4. Renders elements called sliderContainer successfully ", () => {
    expect(wrapper.find(".sliderContainer").exists()).toBeTruthy();
  });

  it("5. Contains a Search component", () => {
    expect(wrapper.find("Search").length).toBe(1);
  });

  it("6. Contains a DataSwitch component", () => {
    expect(wrapper.find("DataSwitch").length).toBe(1);
  });

  it("7. Contains a HelpButton component", () => {
    expect(wrapper.find("HelpButton").length).toBe(1);
  });

  it("8. Renders an img", () => {
    expect(wrapper.find({ alt: "Connected Worlds Logo" }).exists).toBeTruthy();
  });
});

//Test for sub components inside Bottom Bar
describe("<BottomBar/> Mount Tests", () => {
  cytoscapeStore.loadingFinished = true;
  const wrapper = mount(<BottomBar />);

  //Search
  it("1. Result on search box should be empty", () => {
    expect(wrapper.find(Search).state().value).toBe("");
  });

  it("2. There should be 'content' on search box after input some value", () => {
    wrapper
      .find(Search)
      .instance()
      .setState({
        value: "Contents"
      });
    expect(wrapper.find(Search).state().value).toBe("Contents");
  });

  //DataSwitch
  it("3. Value on DataSwitch should be empty", () => {
    expect(wrapper.find(DataSwitch).state().value).toBe("");
  });

  it("4. Call handle change with input years", () => {
    var years = [2001, 2008];
    wrapper
      .find(DataSwitch)
      .instance()
      .handleChange(years);
    expect(wrapper.find(DataSwitch).instance().min).toBe(2001);
    expect(wrapper.find(DataSwitch).instance().max).toBe(2008);
    wrapper.update();
  });

  //HelpButton
  it("5. Test the HelpButton has a svg element for display", () => {
    expect(wrapper.find(HelpButton).find("svg")).toBeTruthy();
  });

  it("6. Test calling onHelpClick of HelpButton ", () => {
    cytoscapeStore.loadingFinished = true;
    spyOn(HelpButton.prototype, "onHelpClick").and.callThrough();
    wrapper
      .find(HelpButton)
      .instance()
      .onHelpClick();
    expect(HelpButton.prototype.onHelpClick).toHaveBeenCalled();
  });
});
