import React from "react";
import { observer } from "mobx-react";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";

const { detect } = require("detect-browser");
const browser = detect();

const firstLetterUpperCase = s => s.charAt(0).toUpperCase() + s.slice(1);
class BrowserDetection extends React.Component {
  constructor(props) {
    super(props);
    this.addWarnNotification = this.addWarnNotification.bind(this);
    this.addSuccessNotification = this.addSuccessNotification.bind(this);
    this.notificationDOMRef = React.createRef();
  }

  addWarnNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Warning!",
      message:
        "You were using [" +
        firstLetterUpperCase(browser.name) +
        "]. To make use of all of the features we recommend using Google Chrome.",
      type: "warning",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 12000 },
      dismissable: { click: true }
    });
  }

  addSuccessNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Supported",
      message:
        "Your browser [" +
        firstLetterUpperCase(browser.name) +
        "] is supported.",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 12000 },
      dismissable: { click: true }
    });
  }

  render() {
    return (
      <div>
        <ReactNotification ref={this.notificationDOMRef} />
        {browser.name.includes("chrome") ? null : (
          <div onLoad={(window.onload = this.addWarnNotification)}></div>
        )}
      </div>
    );
  }
}

export default observer(BrowserDetection);
