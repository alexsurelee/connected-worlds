import React from "react";
import ReactDOM from "react-dom";
import TopBar from "./TopBar";
import { shallow } from "enzyme";
import cytoscapeStore from "../util/CytoscapeStore";

describe("<TopBar/> Tests", () => {
  it("1. Renders topbar without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<TopBar />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders topbar correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<TopBar />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  cytoscapeStore.loadingFinished = true;
  const wrapper = shallow(<TopBar />);

  it('3. An element with class "Navbar" exists', () => {
    expect(wrapper.find(".Navbar").exists()).toBeTruthy();
  });

  it('4. An element with class "Navbar_Item" exists', () => {
    expect(wrapper.find(".Navbar_Item").exists()).toBeTruthy();
  });

  it('5. An element with class "Navbar_Item_Title" exists', () => {
    expect(wrapper.find(".Navbar_Item_Title").exists()).toBeTruthy();
  });

  it('5. An element with class "Navbar_Items" exists', () => {
    expect(wrapper.find(".Navbar_Items").exists()).toBeTruthy();
  });

  it("6. One Views component exists", () => {
    expect(wrapper.find("Views").length).toBe(1);
  });

  it("7. One DetailsPane component exists", () => {
    expect(wrapper.find("DetailsPane").length).toBe(1);
  });

  it("8. Meue image tag has is Navbar_Item class", () => {
    expect(wrapper.find({ alt: "Menu" }).hasClass("Navbar_Item")).toBeTruthy();
  });

  it("9. Meue image tag has is Navbar_Item-toggle class", () => {
    expect(
      wrapper.find({ alt: "Menu" }).hasClass("Navbar_Item-toggle")
    ).toBeTruthy();
  });

  //Test state change...
  it("10. State open should be true after first toggle", () => {
    const wrapper1 = shallow(<TopBar />);
    wrapper1.instance().toggleIcon();
    expect(wrapper1.state().open).toBe(true);
  });

  it("11. State open should be false after double toggle ", () => {
    const wrapper2 = shallow(<TopBar />);
    wrapper2.instance().toggleIcon();
    wrapper2.instance().toggleIcon();
    expect(wrapper2.state().open).toBe(false);
  });

  it("12. If not cytoscapeStore.loadingFinished, return null", () => {
    cytoscapeStore.loadingFinished = false;
    const wrapperNull = shallow(<TopBar />);
    expect(wrapperNull.equals(null)).toBe(true);
  });
});
