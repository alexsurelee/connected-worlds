import React from "react";
import ReactDOM from "react-dom";
import SearchResults from "./SearchResults";

describe("<SearchResults/> Tests", () => {
  it("1. Renders SearchResults without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<SearchResults />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders SearchResults correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<SearchResults />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });
});
