import React from "react";
import "./SearchItem.css";

function SearchItem(props) {
  let tags = props.item.tags;
  // Make sure the tags aren't too long
  if (tags !== undefined) {
    if (tags.length > 20) {
      tags = "...";
    }
  }
  return (
    <li className="ui-menu-item">
      <div
        className="ui-menu-item-wrapper"
        onMouseEnter={() => props.onHover(props.item)}
        onMouseLeave={props.onUnHover}
        onClick={() => props.onSelect(props.item)}
      >
        {props.item.name}
        <span className="tag-show">{tags}</span>
      </div>
    </li>
  );
}

export default SearchItem;
