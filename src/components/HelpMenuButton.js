import React from "react";
import "./HelpMenuButton.css";

class HelpMenuButton extends React.Component {
  render() {
    return (
      <div
        className="help-button-css"
        onClick={() => {
          this.props.onSelect(this.props.name);
        }}
      >
        {this.props.name}
      </div>
    );
  }
}

export default HelpMenuButton;
