import React from "react";
import ReactDOM from "react-dom";
import VideoHelp from "./VideoHelp";
import { shallow } from "enzyme";

describe("<VideoHelp/> Tests", () => {
  it("1. Renders VideoHelp without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<VideoHelp />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders VideoHelp correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<VideoHelp />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<VideoHelp />);

  it("3. VideoHelp return null at start", () => {
    expect(wrapper.equals(null)).toBe(true);
  });

  it("4. renderVideo can be called", () => {
    spyOn(VideoHelp.prototype, "renderVideo").and.callThrough();
    const wrapper = shallow(<VideoHelp />);
    wrapper.instance().renderVideo();
    expect(VideoHelp.prototype.renderVideo).toHaveBeenCalled();
  });
});
