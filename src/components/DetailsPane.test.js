import React from "react";
import ReactDOM from "react-dom";
import DetailsPane from "./DetailsPane";
import RadioButton from "./RadioButton";
import { shallow, mount } from "enzyme";

describe("<DetailsPane/> Tests", () => {
  //test rendering
  it("1. Renders DetailsPane without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DetailsPane />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders DetailsPane correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DetailsPane />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<DetailsPane />);
  it('3. An element with class "nameHeader" exists', () => {
    expect(wrapper.find(".nameHeader").exists()).toBeTruthy();
  });

  it('4. An element with class "expanded" exists', () => {
    expect(wrapper.find(".expanded").exists()).toBeTruthy();
  });

  it("5. h2 contains matched text", () => {
    expect(wrapper.find("h2").text()).toBe("Show details");
  });

  it("6. Test clickHandler(), is should flip the isChecked", () => {
    const wrapper = shallow(<DetailsPane />);
    expect(wrapper.state().isChecked).toBe(true);
    wrapper.instance().clickHandler();
    expect(wrapper.state().isChecked).toBe(false);
  });
});

// using mount to test sub-components of DetailPane

describe("<DetailsPane/> Mount Tests", () => {
  it("1. The radioButton should be selected when button is clicked", () => {
    const wrapper = mount(<DetailsPane />);

    wrapper
      .find(DetailsPane)
      .instance()
      .setState({
        isChecked: true
      });

    wrapper.update();
    const projectsButtonOld = wrapper.find(RadioButton).at(0);
    expect(projectsButtonOld.find("input").props().checked).toEqual(true);
  });

  it("2. The radioButton should be selected when button is clicked", () => {
    const wrapper = mount(<DetailsPane />);

    wrapper
      .find(DetailsPane)
      .instance()
      .setState({
        isChecked: false
      });

    wrapper.update();
    const projectsButtonOld = wrapper.find(RadioButton).at(0);
    expect(projectsButtonOld.find("input").props().checked).toEqual(false);
  });
});
