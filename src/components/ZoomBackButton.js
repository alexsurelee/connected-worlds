import React from "react";
import cytoscapeStore from "../util/CytoscapeStore";
import ProjectLayout from "../layouts/ProjectLayout";
import CollaboratorLayout from "../layouts/CollaboratorLayout";
import ProgrammeLayout from "../layouts/ProgrammeLayout";
import "./ZoomBackButton.css";

class ZoomBackButton extends React.Component {
  constructor() {
    super();
    this.state = {
      selected: ""
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const selected = window.viewsComponent.returnState();
    this.setState({
      selected: selected
    });

    if (selected.includes("Semi-Circle")) {
      cytoscapeStore.layouts = ProjectLayout.getLayout();
    } else if (selected.includes("Groups")) {
      cytoscapeStore.layouts = ProgrammeLayout.getLayout();
    } else if (selected.includes("Collaborators")) {
      cytoscapeStore.layouts = CollaboratorLayout.getLayout();
    }
  }

  render() {
    if (!cytoscapeStore.loadingFinished) {
      return null;
    }
    return (
      <div className="zoomBackButton-css" onClick={this.onClick}>
        {" "}
        Back to Origin{" "}
      </div>
    );
  }
}

export default ZoomBackButton;
