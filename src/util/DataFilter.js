// This class gets the data and finds name filters for cytoscape
class DataFilter {
  parseElementsToGetFilterNames(graphP) {
    const filterNames = graphP.then(graphData => {
      const set = new Set();
      for (let i = 0; i < graphData.length; i++) {
        const dataInstance = graphData[i];
        set.add(dataInstance.data.type);
      }
      return [...set];
    });
    return filterNames;
  }
}

const datafilter = new DataFilter();

export default datafilter;
