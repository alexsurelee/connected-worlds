import filter from "../util/DataFilter";

// This class gets the data and stores it to be called by cytoscape
class NetworkRequest {
  constructor() {
    // Get the json data from the API
    this.graphP = new Promise((resolve, reject) => {
      let localApiStat = 0;
      fetch("http://localhost:8080/status").then(
        response => (localApiStat = response.status),
        error => (localApiStat = error.status)
      );

      function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }

      function getPathname() {
        // retrieve the current URL
        const url = window.location.href;
        // Split the url every instance of '/'
        const path = new URL(url).pathname.split("/");
        // Keep the entry where the unique ID is stored
        const pathname = path[2];
        return pathname;
      }

      async function retry(domain) {
        await sleep(1000);
        return fetch(domain + "/sheets?version=cached&id=" + getPathname())
          .then(response => response.json())
          .then(jsonData => {
            // Once the json data has been received return it
            resolve(jsonData);
          })
          .catch(error => {
            console.error(error);
          });
      }

      // Set the api endpoint based on what's responding
      let fetch_location = "";
      if (localApiStat === 200) {
        fetch_location = "http://localhost/8080";
      } else if (window.location.href.includes("staging")) {
        fetch_location = "https://api-staging.connected-worlds.host";
      } else {
        fetch_location = "https://api-staging.connected-worlds.host";
      }
      if (getPathname()) {
        // Download a specific google sheet
        return fetch(fetch_location + "/sheets?version=new&id=" + getPathname())
          .then(response => {
            response
              .json()
              .then(jsonData => {
                resolve(jsonData);
              })
              .catch(error => {
                retry(fetch_location);
              });
          })
          .catch(error => {
            retry(fetch_location);
          });
      } else {
        // Display the default data
        return fetch("https://api-staging.connected-worlds.host/default/nodes")
          .then(response => response.json())
          .then(jsonData => {
            // Once the json data has been received return it
            resolve(jsonData);
          })
          .catch(error => {
            console.error(error);
          });
      }
    });

    this.filterNames = filter.parseElementsToGetFilterNames(this.getGraphP());
  }

  getGraphP() {
    return this.graphP;
  }

  getFilterNames() {
    return this.filterNames;
  }
}

const networkRequest = new NetworkRequest();

export default networkRequest;
